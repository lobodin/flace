dev:
	supervisor app.js

css:
	compass watch frontend/stylesheets

test:
	mocha -R spec backend/tests.js

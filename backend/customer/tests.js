var async = require('async')
  , expect = require('expect.js')
  , common = require('../common/test_utils');

var GPS = [131.8948, 43.12555];
var BEIJING = [116.339958, 39.976446];

describe('/api/deals', function() {
  var URL = 'deals';

  before(function(done) {
    async.series([
      function(cb) {
        common.flushDeals(cb);
      },
      function(cb) {
        common.importCurrentDeal('current1', cb);
      },
      function(cb) {
        common.importCurrentDeal('current2', cb);
      },
      function(cb) {
        common.importNotStartedDeal(cb);
      },
      function(cb) {
        common.importExpiredDeal(cb);
      },
      function(cb) {
        common.importNoUnitsDeal(cb);
      }
    ], done);
  });

  after(function(done) {
    common.flushDeals(done);
  });

  function expectCurrentDeal(deal, name) {
    var now = new Date().getTime();
    expect(deal).to.have.property('name', name);
    expect(deal).to.have.property('description', name);
    expect(deal).to.have.property('units_total', 2);
    expect(deal).to.have.property('units_left', 2);
    expect(deal).to.have.property('start');
    expect(deal).to.have.property('end');
    expect(now).to.be.within(Date.parse(deal.start), Date.parse(deal.end));
  }

  describe('when there are current deals', function() {
    it('should load those deals', function(done) {
      common.get(URL, { latitude: GPS[1], longitude: GPS[0] }, function(err, res) {
        expect(res.body).to.have.length(2);
        expectCurrentDeal(res.body[0], 'current1');
        expectCurrentDeal(res.body[1], 'current2');
        done();
      });
    });

    describe('and limit is provided', function() {
      it('should load limited results', function(done) {
        common.get(URL, { latitude: GPS[1], longitude: GPS[0], limit: 1 }, function(err, res) {
          expect(res.body).to.have.length(1);
          expectCurrentDeal(res.body[0], 'current1');
          done();
        });
      });
    });

    describe('and offset is provided', function() {
      it('should load offset results', function(done) {
        common.get(URL, { latitude: GPS[1], longitude: GPS[0], offset: 1 }, function(err, res) {
          expect(res.body).to.have.length(1);
          expectCurrentDeal(res.body[0], 'current2');
          done();
        });
      });
    });

    describe('and radius is provided', function() {
      it('should load results within given radius', function(done) {
        common.get(URL, { latitude: GPS[1], longitude: GPS[0], radius: 5 }, function(err, res) {
          expect(res.body).to.have.length(2);
          done();
        });
      });
    });
  });

  describe('when there are no current deals', function() {
    describe('anywhere near', function() {
      it('should load empty array', function(done) {
        common.get(URL, { latitude: BEIJING[1], longitude: BEIJING[0] }, function(err, res) {
          expect(res.body).to.have.length(0);
          done();
        });
      });
    });

    describe('within given radius', function() {
      it('should load empty array', function(done) {
        common.get(URL, { latitude: GPS[1], longitude: GPS[0], radius: 1 }, function(err, res) {
          expect(res.body).to.have.length(0);
          done();
        });
      });
    });
  });
});

describe('/api/deals/past', function() {
  var URL = 'deals/past';

  before(function(done) {
    async.series([
      function(cb) {
        common.flushDeals(cb);
      },
      function(cb) {
        common.importCurrentDeal('current', cb);
      },
      function(cb) {
        common.importNotStartedDeal(cb);
      },
      function(cb) {
        common.importNoUnitsDeal(cb);
      },
      function(cb) {
        common.importExpiredDeal(cb);
      }
    ], done);
  });

  after(function(done) {
    common.flushDeals(done);
  });

  function expectPastDeal(deal, name) {
    expect(deal).to.have.property('name', name);
    expect(deal).to.have.property('description', name);
    expect(deal).to.have.property('start');
    expect(deal).to.have.property('end');
    expect(deal).to.not.have.property('units_total');
    expect(deal).to.not.have.property('units_left');
  }

  describe('when there are past deals', function() {
    it('should load those deals', function(done) {
      common.get(URL, { latitude: GPS[1], longitude: GPS[0] }, function(err, res) {
        expect(res.body).to.have.length(2);
        expectPastDeal(res.body[0], 'no_units', 2, 0);
        expectPastDeal(res.body[1], 'expired', 2, 2);
        expect(new Date().getTime()).to.be.greaterThan(Date.parse(res.body[1].end));
        done();
      });
    });

    describe('when limit is provided', function(done) {
      it('should load limited results', function(done) {
        common.get(URL, { latitude: GPS[1], longitude: GPS[0], limit: 1 }, function(err, res) {
          expect(res.body).to.have.length(1);
          expectPastDeal(res.body[0], 'no_units', 2, 0);
          done();
        });
      });
    });
  });

  describe('when there are NO past deals', function() {
    it('should load empty array', function(done) {
      common.get(URL, { latitude: BEIJING[1], longitude: BEIJING[0] }, function(err, res) {
        expect(res.body).to.have.length(0);
        done();
      });
    });
  });
});

var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var dealSchema = new Schema({
  name: String,
  description: String,
  start: Date,
  end: Date,
  image_url: String,
  units_total: Number,
  units_left: Number,
  winners: [{ type: ObjectId, ref: 'User' }],
  business: { type: ObjectId, ref: 'Business' },
  coordinates: Schema.Types.Mixed
});

var userSchema = new Schema({
  device_id: String,
  deals: [{ type: ObjectId, ref: 'Deal' }]
});

dealSchema.index({ coordinates: '2d', start: 1, end: 1, units_left: 1 });

mongoose.model('Deal', dealSchema);
mongoose.model('User', userSchema);

var api = require('./api');

module.exports = function(app) {
  app.get('/api/deals', api.deals);
  app.get('/api/deals/past', api.pastDeals);

  app.get('/', function(req, res) {
    var params = {
      longitude: req.cookies.longitude,
      latitude: req.cookies.latitude,
      radius: req.cookies.radius
    };
    api.dealsJSON(params, function(data) {
      res.render('customer', { deals: JSON.stringify(data) });
    });
  });
};

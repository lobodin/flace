var mongoose = require('mongoose')
  , sanitize = require('validator').sanitize
  , check = require('validator').check
  , auth = require('../common/api_utils').auth;

var Deal = mongoose.model('Deal');

function distance(km) {
  return km / 111.12;
}

var DEFAULT_DISTANCE = distance(50);

function location(query) {
  var longitude = sanitize(query.longitude).toFloat();
  var latitude = sanitize(query.latitude).toFloat();
  var radius = sanitize(query.radius).toInt();

  check(longitude).isFloat();
  check(latitude).isFloat();

  try {
    check(radius).isInt().max(500).min(1);
    radius = distance(radius);
  } catch (e) {
    radius = DEFAULT_DISTANCE;
  }

  return {
    $geoWithin: {
      $center: [[longitude, latitude], radius]
    }
  }
}

exports.dealsJSON = function(query, callback) {
  var currentDate = new Date();

  var limit = sanitize(query.limit).toInt();
  try {
    check(limit).isInt().max(100).min(1);
  } catch (e) {
    limit = 50;
  }

  var offset = sanitize(query.offset).toInt();
  try {
    check(offset).isInt().max(10000).min(1);
  } catch (e) {
    offset = 0;
  }

  try {
    var coordinates = location(query);
  } catch (e) {
    return callback([]);
  }

  Deal.find({
    coordinates: coordinates,
    start: { $lte: currentDate },
    end: { $gte: currentDate },
    units_left: { $gt: 0 }
  }).populate('business', 'name address')
    .sort('end')
    .limit(limit)
    .skip(offset)
    .exec(function(err, deals) {
      if (err)
        return callback({ error: err.name });
      return callback(deals);
    });
}

exports.deals = function(req, res) {
  exports.dealsJSON(req.query, function(data) {
    res.json(data);
  });
};

exports.pastDeals = function(req, res) {
  var currentDate = new Date();

  var limit = sanitize(req.query.limit).toInt();
  try {
    check(limit).isInt().max(100).min(1);
  } catch (e) {
    limit = 50;
  }

  try {
    var coordinates = location(req.query);
  } catch (e) {
    return callback([]);
  }

  Deal.find({
    coordinates: coordinates,
    $or: [
      { end: { $lt: currentDate } },
      { units_left: 0 }
    ]
  }).populate('business', 'name address')
    .sort('-end')
    .limit(limit)
    .select('-units_total -units_left')
    .exec(function(err, deals) {
      if (err)
        return res.json({ error: err.name });
      res.json(deals);
    });
};

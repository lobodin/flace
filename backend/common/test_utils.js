var request = require('supertest')
  , expect = require('expect.js')
  , async = require('async')
  , mongoose = require('mongoose');

var Deal = mongoose.model('Deal');
var Business = mongoose.model('Business');

var URL = 'http://localhost:' + process.env.PORT;
var GPS = [131.878944, 43.120546]; // Vladivostok location
var oneHourOffset = 60 * 60 * 1000;
var businessFixture = require('../../fixtures/business.json');

function newDeal(name, startOffset, endOffset, unitsLeft, callback) {
  var now = new Date().getTime();
  var deal = new Deal({
    name: name,
    description: name,
    start: new Date(now + startOffset),
    end: new Date(now + endOffset),
    units_total: 2,
    units_left: unitsLeft,
    coordinates: GPS
  });
  deal.save(callback);
};

function newBusiness(fixture, isVerified, callback) {
  if (isVerified)
    fixture.verified = true;
  var business = new Business(fixture);
  business.save(callback);
};

exports.get = function(action, query, callback) {
  request(URL)
    .get('/api/' + action)
    .query(query)
    .expect(200)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      callback(err, res);
    });
};

exports.post = function(action, query, callback) {
  request(URL)
    .post('/api/' + action)
    .send(query)
    .expect(200)
    .expect('Content-Type', /json/)
    .end(function(err, res) {
      callback(err, res);
    });
};

exports.importCurrentDeal = function(name, callback) {
  newDeal(name, -oneHourOffset, oneHourOffset, 2, callback);
};

exports.importNotStartedDeal = function(callback) {
  newDeal('not_started', oneHourOffset, 2 * oneHourOffset, 2, callback);
};

exports.importExpiredDeal = function(callback) {
  newDeal('expired', -2 * oneHourOffset, -oneHourOffset, 2, callback);
};

exports.importNoUnitsDeal = function(callback) {
  newDeal('no_units', -oneHourOffset, oneHourOffset, 0, callback);
};

exports.importBusiness = function(i, callback) {
  newBusiness(businessFixture[i], false, callback);
};

exports.importVerifiedBusiness = function(i, callback) {
  newBusiness(businessFixture[i], true, callback);
};

exports.flushDeals = function(callback) {
  Deal.remove(callback);
};

exports.flushBusiness = function(callback) {
  Business.remove(callback);
};

exports.flush = function(callback) {
  async.series([
    function(cb) {
      exports.flushBusiness(cb);
    },
    function(cb) {
      exports.flushDeals(cb);
    }
  ], callback);
};

exports.addDealToBusiness = function(id, name, callback) {
  Deal.findOne({ name: name }, function(err, deal) {
    deal.business = id;
    deal.save(function(err) {
      Business.findById(id, function(err, biz) {
        biz.deals.push(deal);
        biz.save(callback)
      });
    });
  });
};

exports.loginBusiness = function (id, sid, callback) {
  Business.findById(id, function (err, biz) {
    biz.sids.push(sid);
    biz.save(callback);
  });
};

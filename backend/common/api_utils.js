var mongoose = require('mongoose');

var Business = mongoose.model('Business');

exports.auth = function(sid, callback) {
  Business.findBySid(sid, callback);
};

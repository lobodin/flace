process.env.NODE_ENV = 'test';
process.env.PORT = 3002;

require('../app');
require('./common/tests');
require('./customer/tests');
require('./business/tests');

var mongoose = require('mongoose');
var crypto = require('crypto');

var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var businessSchema = new Schema({
  name: String,
  description: String,
  email: { type: String, unique: true },
  phone_number: String,
  address: String,
  logo_id: String,
  deals: [{ type: ObjectId, ref: 'Deal' }],
  password: String,
  salt: String,
  verified: { type: Boolean, default: false },
  sids: [String],
  coordinates: Schema.Types.Mixed
});

businessSchema.method('makeSalt', function() {
  return Math.round((new Date().valueOf() * Math.random())) + '';
});

businessSchema.method('encryptPassword', function(plainText) {
  return crypto.createHmac('sha1', this.salt).update(plainText).digest('hex');
});

businessSchema.pre('save', function(next) {
  if (!this.isModified('password'))
    return next();
  this.salt = this.makeSalt();
  this.password = this.encryptPassword(this.password);
  next();
});

businessSchema.method('authenticate', function(plainText) {
  return this.encryptPassword(plainText) === this.password;
})

businessSchema.static('login', function(email, password, callback) {
  this.findOne({ email: email, verified: true }, function(err, business) {
    if (err)
      return callback(err, null);
    if (!business || !business.authenticate(password))
      return callback(new mongoose.Error('invalid_credentials'), null);
    return callback(null, business);
  });
});

businessSchema.static('findBySid', function(sid, callback) {
  this.findOne({ sids: sid, verified: true }, { "_id": 1 }, function(err, business) {
    if (err)
      return callback(err, null);
    if (!business)
      return callback(null, null);
    return callback(null, business);
  });
});

businessSchema.index({ email: 1, verified: 1 });
businessSchema.index({ sids: 1, verified: 1 }, { expireAfterSeconds: 30 * 24 * 60 * 60 });

mongoose.model('Business', businessSchema);

var async = require('async')
  , expect = require('expect.js')
  , util = require('util')
  , common = require('../common/test_utils')
  , businessFixtures = require('../../fixtures/business.json')
  , dealsFixtures = require('../../fixtures/deals.json')
  , moment = require('moment');

var SID = new Array(33).join('a');
var INVALID_SID = new Array(33).join('b');

function testInvalidSid(URL, is_post) {
  function call(cb) {
    if (is_post)
      common.post(URL, { sid: INVALID_SID }, cb);
    else
      common.get(URL, { sid: INVALID_SID }, cb);
  }

  describe('when business with such sid DOES NOT exist', function() {
    it('should return `invalid_sid`', function(done) {
      call(function(err, res) {
        expect(res.body).to.have.property('error', 'invalid_sid');
        done();
      });
    });
  });
}

describe('`/api/business/register`', function() {
  var URL = 'business/register';

  afterEach(function(done) {
    common.flushBusiness(done);
  });

  describe('when some parameters are missing', function() {
    it('should return `validation_error`', function(done) {
      common.post(URL, {}, function(err, res) {
        expect(res.body).to.have.property('error', 'validation_error');
        done();
      });
    });
  });

  describe('when all required parameters are present and correct', function() {
    it('should return ok', function(done) {
      common.post(URL, businessFixtures[0], function(err, res) {
        done();
      });
    });
  });

  describe('when there is already business with such email', function() {
    before(function(done) {
      common.importBusiness(0, done);
    });

    it('should return `already_exists`', function(done) {
      common.post(URL, businessFixtures[0], function(err, res) {
        expect(res.body).to.have.property('error', 'already_exists');
        done();
      });
    });
  });
});

describe('`/api/business/login`', function() {
  var URL = 'business/login';
  var business = businessFixtures[0];

  afterEach(function(done) {
    common.flushBusiness(done);
  });

  describe('when business is NOT verified and correct email/password is provided', function() {
    before(function(done) {
      common.importBusiness(0, done);
    });

    it('should return `invalid_credentials`', function(done) {
      common.get(URL, { email: business.email, password: business.password }, function(err, res) {
        expect(res.body).to.have.property('error', 'invalid_credentials');
        done();
      });
    });
  });

  describe('when business is verified and correct email/password is provided', function() {
    before(function(done) {
      common.importVerifiedBusiness(0, done);
    });

    it('should return SID', function(done) {
      common.get(URL, { email: business.email, password: business.password }, function(err, res) {
        expect(res.body.sid).to.have.length(32);
        done();
      });
    });
  });

  describe('when there is no business with such email', function() {
    it('should return `invalid_credentials`', function(done) {
      common.get(URL, { email: 'login@example.ru', password: business.password }, function(err, res) {
        expect(res.body).to.have.property('error', 'invalid_credentials');
        done();
      });
    });
  });

  describe('when password is wrong', function() {
    before(function(done) {
      common.importVerifiedBusiness(0, done);
    });

    it('should return `invalid_credentials`', function(done) {
      common.get(URL, { email: business.email, password: "password" }, function(err, res) {
        expect(res.body).to.have.property('error', 'invalid_credentials');
        done();
      });
    });
  });
});

describe('`/api/business`', function() {
  var URL = 'business';
  var business = businessFixtures[0];

  before(function(done) {
    async.series([
      function(cb) {
        common.flushBusiness(cb);
      },
      function(cb) {
        common.importVerifiedBusiness(0, cb);
      },
      function(cb) {
        common.loginBusiness(business._id, SID, cb);
      },
    ], done);
  });

  after(function(done) {
    common.flushBusiness(done);
  });

  testInvalidSid(URL);

  describe('when business with such sid exists', function() {
    it('should return business info', function(done) {
      common.get(URL, { sid: SID }, function(err, res) {
        var biz = res.body;
        expect(biz.name).to.eql(business.name);
        expect(biz.description).to.eql(business.description);
        expect(biz.email).to.eql(business.email);
        expect(biz.address).to.eql(business.address);
        expect(biz.phone_number).to.eql(business.phone_number);
        expect(biz.deals).to.be.empty();
        done();
      });
    });
  });
});

describe('`/api/business/deals`', function() {
  var businessId = businessFixtures[0]._id;
  var dealName = 'current';
  var URL = 'business/deals';

  before(function(done) {
    async.series([
      function(cb) {
        common.flushDeals(cb);
      },
      function(cb) {
        common.flushBusiness(cb);
      },
      function(cb) {
        common.importVerifiedBusiness(0, cb);
      },
      function(cb) {
        common.importCurrentDeal(dealName, cb);
      },
      function(cb) {
        common.addDealToBusiness(businessId, dealName, cb);
      },
      function(cb) {
        common.loginBusiness(businessId, SID, cb);
      },
    ], done);
  });

  testInvalidSid(URL);

  describe('when business has one deal', function() {
    it('should return that deal', function(done) {
      common.get(URL, { sid: SID }, function(err, res) {
        expect(res.body).to.have.length(1);
        expect(res.body[0]).to.have.property('name', 'current');
        done();
      });
    });
  });

  describe('when business DOES NOT have deals', function() {
    before(function(done) {
      common.flushDeals(done);
    });

    it('should return empty array', function(done) {
      common.get(URL, { sid: SID }, function(err, res) {
        expect(res.body).to.be.empty();
        done();
      })
    });
  })
});

describe('`/api/business/deals/new`', function() {
  var business = businessFixtures[0];
  var deal = dealsFixtures[0];
  var URL = 'business/deals/new';
  deal.sid = SID;
  deal.start = moment().add('days', 1).format('MM/DD/YYYY');
  deal.end = moment().add('days', 2).format('MM/DD/YYYY');

  before(function(done) {
    async.series([
      function(cb) {
        common.flush(cb);
      },
      function(cb) {
        common.importVerifiedBusiness(0, cb);
      },
      function(cb) {
        common.loginBusiness(business._id, SID, cb);
      }
    ], done);
  });

  after(function(done) {
    common.flush(done);
  });

  testInvalidSid(URL, true);

  describe('when some parameters are missing', function() {
    it('should return `validation_error`', function(done) {
      common.post(URL, { sid: SID }, function(err, res) {
        expect(res.body).to.have.property('error', 'validation_error');
        done();
      });
    });
  });

  describe('when deal is outdated', function() {
    it('should return `validation_error`', function(done) {
      var outdated = dealsFixtures[1];
      outdated.start = moment().subtract('years', 10);
      outdated.end = moment().subtract('years', 1);
      outdated.sid = SID;
      common.post(URL, outdated, function(err, res) {
        expect(res.body).to.have.property('error', 'validation_error');
        done();
      });
    });
  });

  describe('when all parameters are present and correct', function() {
    it('should create deal', function(done) {
      async.waterfall([
        function(cb) {
          common.post(URL, deal, cb);
        },
        function(res, cb) {
          common.get('business/deals', { sid: SID }, cb);
        },
        function(res, cb) {
          expect(res.body).to.have.length(1);
          expect(res.body[0]).to.have.property('name', 'Free coffee');
          done();
        }
      ]);
    });
  });

});

describe('`/api/business/update`', function() {
  var businessId = businessFixtures[0]._id;
  var URL = 'business/update';

  before(function(done) {
    async.series([
      function(cb) {
        common.flushDeals(cb);
      },
      function(cb) {
        common.flushBusiness(cb);
      },
      function(cb) {
        common.importVerifiedBusiness(0, cb);
      },
      function(cb) {
        common.loginBusiness(businessId, SID, cb);
      },
    ], done);
  });

  testInvalidSid(URL, true);

  describe('when business with such sid exists', function() {
    it('should update business info', function(done) {
      async.waterfall([
        function(cb) {
          common.post(URL, { sid: SID, description: 'Free Wi-Fi' }, cb);
        },
        function(res, cb) {
          common.get('business', { sid: SID }, cb);
        },
        function(res, cb) {
          expect(res.body).to.have.property('description', 'Free Wi-Fi');
          done();
        }
      ]);
    });
  });
});

describe('`/api/business/deals/update`', function() {
  var businessId = businessFixtures[0]._id;
  var URL = 'business/deals/update';

  before(function(done) {
    async.series([
      function(cb) {
        common.flushDeals(cb);
      },
      function(cb) {
        common.flushBusiness(cb);
      },
      function(cb) {
        common.importVerifiedBusiness(0, cb);
      },
      function(cb) {
        common.importNotStartedDeal(cb);
      },
      function(cb) {
        common.addDealToBusiness(businessId, 'not_started', cb);
      },
      function(cb) {
        common.loginBusiness(businessId, SID, cb);
      },
    ], done);
  });

  testInvalidSid(URL, true);

  describe('when business has deal and it DID NOT start yet', function() {
    it('should update it', function(done) {
      async.waterfall([
        function(cb) {
          common.get('business/deals', { sid: SID }, cb)
        },
        function(res, cb) {
          var deal = res.body[0];
          deal.name = 'New Name';
          deal.sid = SID;
          common.post(URL, deal, cb);
        },
        function(res, cb) {
          common.get('business/deals', { sid: SID }, cb);
        },
        function(res, cb) {
          expect(res.body[0]).to.have.property('name', 'New Name');
          done();
        }
      ]);
    });
  });

  describe('when business has deal and it has been started already', function() {
    before(function(done) {
      async.series([
        function(cb) {
          common.flushDeals(cb);
        },
        function(cb) {
          common.importCurrentDeal('current', cb);
        },
        function(cb) {
          common.addDealToBusiness(businessId, 'current', cb);
        },
      ], done);
    });

    it('should return `invalid_deal`', function(done) {
      async.waterfall([
        function(cb) {
          common.get('business/deals', { sid: SID }, cb)
        },
        function(res, cb) {
          var deal = res.body[0];
          deal.name = 'New Name';
          deal.sid = SID;
          common.post(URL, deal, cb);
        },
        function(res, cb) {
          expect(res.body).to.have.property('error', 'invalid_deal')
          common.get('business/deals', { sid: SID }, cb);
          done();
        },
      ]);
    });
  });
});

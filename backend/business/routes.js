var api = require('./api');

module.exports = function(app) {
  app.post('/api/business/register', api.register);
  app.get('/api/business/login', api.login);
  app.post('/api/business/update', api.updateBusiness);
  app.get('/api/business', api.show);
  app.get('/api/business/deals', api.deals);
  app.post('/api/business/deals/new', api.newDeal);
  app.post('/api/business/deals/update', api.updateDeal);
};

var mongoose = require('mongoose')
  , crypto = require('crypto')
  , sanitize = require('validator').sanitize
  , check = require('validator').check
  , auth = require('../common/api_utils').auth
  , moment = require('moment')
  , _ = require('underscore');

var Business = mongoose.model('Business');
var Deal = mongoose.model('Deal');

function invalidSid(res) {
  return res.json({ error: 'invalid_sid' });
}

function isDatePast(date) {
  return moment(date).isBefore(moment().format());
}

function validateDeal(o) {
  try {
    check(o.name).len(6, 100);
    check(o.description).len(6, 300);
    check(o.start).isDate();
    check(o.end).isDate().isAfter(o.start);
    check(o.units_total).isInt().min(1);
  } catch (e) {
    return false;
  }

  return !isDatePast(o.start) && !isDatePast(o.end);
}

exports.register = function(req, res) {
  var o = {
    'email': req.body.email,
    'password': req.body.password,
    'name': req.body.name,
    'description': req.body.description,
    'address': req.body.address,
    'phone_number': req.body.phone_number,
    'coordinates': [req.body.longitude, req.body.latitude],
  };
  try {
    check(o.email).isEmail();
    check(o.password).len(6, 30);
    check(o.name).len(6, 50);
    check(o.description).len(6, 300);
    check(o.address).len(6, 100);
    check(o.phone_number).len(6, 20);
    check(o.coordinates[0]).isFloat();
    check(o.coordinates[1]).isFloat();
  } catch (e) {
    return res.json({ error: 'validation_error' });
  }

  var business = new Business(o);
  business.save(function(err) {
    if (err) {
      if (err.name === 'MongoError' && err.code === 11000)
        return res.json({ error: 'already_exists' });
      return res.json({ error: err.name });
    }
    res.json({});
  });
};

exports.login = function(req, res) {
  Business.login(req.query.email, req.query.password, function(err, business) {
    if (err) {
      if (err.message == 'invalid_credentials')
        return res.json({ error: 'invalid_credentials' });
      return res.json({ error: err.name });
    }

    crypto.randomBytes(16, function(ex, buf) {
      var sid = buf.toString('hex');
      business.sids.push();

      res.cookie('sid', business.sid);

      business.save(function(err, obj) {
        if (err)
          return res.json({ error: err.name });
        res.json({ sid: sid });
      });
    });
  });
};

exports.show = function(req, res) {
  Business.findOne({
    sids: req.query.sid,
    verified: true
  }).select('name description email phone_number address deals')
    .populate('deals', 'name description start end units_total units_left')
    .exec(function(err, biz) {
      if (err)
        return res.json({ error: err.name });
      if (!biz)
        return invalidSid(res);
      res.json(biz);
    });
};

exports.deals = function(req, res) {
  Business.findOne({
    sids: req.query.sid,
    verified: true
  }).select('deals')
    .populate('deals', '_id name description start end units_total units_left')
    .exec(function(err, biz) {
      if (err)
        return res.json({ error: err.name });
      if (!biz)
        return invalidSid(res);
      res.json(biz.deals);
    });
};

exports.newDeal = function(req, res) {
  Business.findOne({
    sids: req.body.sid,
    verified: true
  }, '_id deals', function(err, biz) {
    if (err)
      return res.json({ error: err.name });
    if (!biz)
      return invalidSid(res);

    var o = {
      'name': req.body.name,
      'description': req.body.description,
      'start': req.body.start,
      'end': req.body.end,
      'units_total': req.body.units_total,
      'units_left': req.body.units_total,
      'business': biz._id,
      'coordinates': biz.coordinates
    };

    if (req.body.image_url)
      o['image_url'] = req.body.image_url;

    if (!validateDeal(o))
      return res.json({ error: 'validation_error' });

    var deal = new Deal(o);
    deal.save(function(err) {
      if (err)
        return res.json({ error: err.name });

      biz.deals.push(deal);
      biz.save();
      res.json({});
    });
  });
};

exports.updateBusiness = function(req, res) {
  Business.findOne({
    sids: req.body.sid,
    verified: true
  }, '_id description', function(err, biz) {
    if (err)
      return res.json({ error: err.name });
    if (!biz)
      return invalidSid(res);

    var o = {
      'description': req.body.description,
    };
    try {
      check(o.description).len(6, 300);
    } catch (e) {
      return res.json({ error: 'validation_error' });
    }

    biz.description = o.description;
    biz.save(function(err) {
      if (err)
        return res.json({ error: err.name });
      res.json({});
    });
  });
};

exports.updateDeal = function(req, res) {
  Business.findOne({
    sids: req.body.sid,
    verified: true
  }).select('_id deals')
    .populate('deals', null, { _id: req.body._id })
    .exec(function(err, biz) {
      if (err)
        return res.json({ error: err.name });
      if (!biz)
        return invalidSid(res);

      var deal = biz.deals[0];
      if (!deal || isDatePast(deal.start))
        return res.json({ error: 'invalid_deal' });

      var o = {
        'name': req.body.name,
        'description': req.body.description,
        'start': req.body.start,
        'end': req.body.end,
        'units_total': req.body.units_total,
        'units_left': req.body.units_total,
      };

      if (!validateDeal(o))
        return res.json({ error: 'validation_error' });

      _.each(o, function(value, key) {
        deal[key] = value;
      });

      deal.save(function(err) {
        if (err)
          return res.json({ error: err.name });
        res.json({});
      });
  });
};

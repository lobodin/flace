var express = require('express');
var mongoose = require('mongoose');

var app = express();

app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.logger('dev'));
app.use(express.cookieParser('supersecret'));
app.use(express.bodyParser());
app.use(app.router);
app.use('/public', express.static(__dirname + '/frontend'));

app.configure('production', function() {
  mongoose.connect('mongodb://admin:goddamPASS@dharma.mongohq.com:10068/flace');
});

app.configure('development', function() {
  app.use(express.errorHandler());
  mongoose.connect('localhost', 'dev');
});

app.configure('test', function() {
  mongoose.connect('localhost', 'test');
});

require('./backend/common/models');
require('./backend/customer/models');
require('./backend/business/models');

require('./backend/common/routes')(app);
require('./backend/customer/routes')(app);
require('./backend/business/routes')(app);

app.listen(app.get('port'));

requirejs.config({ 'paths': {
    jquery: 'libs/jquery',
    underscore: 'libs/underscore',
    backbone: 'libs/backbone',
    cookie: 'libs/jquery.cookie',
    slider: 'libs/html5slider'
  },
  'shim': {
    backbone: {
      deps: ['underscore', 'jquery'],
      exports: 'Backbone'
    },
    underscore: {
      exports: '_'
    },
    cookie: {
      deps: ['jquery']
    }
  }
});

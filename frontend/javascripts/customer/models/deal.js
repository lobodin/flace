define(function(require) {
  var Deal = require('backbone').Model.extend({
    idAttribute: '_id',

    initialize: function() {
      var end = new Date(this.get('end'));
      var now = new Date();
      var minutes = Math.floor((end - now) / 1000 / 60) + 1;

      var hr = Math.floor(minutes / 60);
      var min = minutes % 60;

      var time = '';
      if (hr != 0)
        time += hr + 'h ';
      if (min != 0)
        time += min + 'min';
      this.set('time_left', time);
    }
  });

  return Deal;
});

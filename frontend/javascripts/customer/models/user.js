define(function(require) {
  var cookie = require('cookie');
  var $ = require('jquery');

  var MIN_RADIUS = 1;
  var MAX_RADIUS = 500;

  var User = require('backbone').Model.extend({

    initialize: function() {
      var r = this.validateRadius(parseInt($.cookie('radius')));
      this.set('radius', r ? r : 50);
    },

    validateRadius: function(r) {
      if (r < MIN_RADIUS)
        r = MIN_RADIUS;
      else if (r > MAX_RADIUS)
        r = MAX_RADIUS;
      return r;
    },

    setRadius: function(r) {
      var r = this.validateRadius(r);
      $.cookie('radius', r);
      this.set('radius', r);
    },

    setCoordinates: function(longitude, latitude) {
      this.set('longitude', longitude);
      this.set('latitude', latitude);
    }

  });

  return new User;
});

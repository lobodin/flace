define(function(require) {
  var Backbone = require('backbone');
  var cookie = require('cookie');
  var $ = require('jquery');

  var DealsView = require('./views/deals');
  var RadiusView = require('./views/radius');
  var dealsCollection = require('./collections/deals');
  var user = require('./models/user');

  function initDeals(lon, lat) {
    user.setCoordinates(lon, lat);

    function fetch() {
      dealsCollection.refresh();
    }

    setInterval(function() {
      fetch();
    }, 3000);

    if (INITIAL_DEALS.length == 0)
      fetch();
  }

  function getLocation(location) {
    var lon = location.coords.longitude;
    var lat = location.coords.latitude;
    $.cookie('longitude', lon);
    $.cookie('latitude', lat);
    initDeals(lon, lat);
  }

  var Router = Backbone.Router.extend({
    initialize: function() {
      Backbone.history.start();
      this.dealsView = new DealsView;
      this.radiusView = new RadiusView;

      var lon = $.cookie('longitude');
      var lat = $.cookie('latitude');
      if (lon && lat)
        initDeals(lon, lat);
      else
        navigator.geolocation.getCurrentPosition(getLocation);
    },

    routes: {
      '': 'home'
    },

    home: function() {
    }

  });

  var app = new Router();
});

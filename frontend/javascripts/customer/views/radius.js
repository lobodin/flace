define(function(require) {
  require('slider');
  var dealsCollection = require('../collections/deals');
  var user = require('../models/user');

  var RadiusView = require('backbone').View.extend({
    el: '#radius',

    initialize: function() {
      this.render();
    },

    render: function() {
      var r = user.get('radius');
      this.$('i').text(r + 'km');
      this.$('input').val(r);
    },

    events: {
      'click i': 'showSlider',
      'mouseup input': 'changeRadius',
      'touchend input': 'changeRadius'
    },

    changeRadius: function() {
      user.setRadius(this.$('input').val());
      this.$('input').hide();
      this.$('i').show();
      this.$el.css('background', '#000');
      this.render();
      dealsCollection.refresh();
    },

    showSlider: function() {
      this.$('i').hide();
      this.$('input').show();
      this.$el.css('background', 'none');
    }

  });

  return RadiusView;
});

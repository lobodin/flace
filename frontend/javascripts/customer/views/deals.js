define(function(require) {
  var _ = require('underscore');

  var DealView = require('./deal');
  var dealsCollection = require('../collections/deals');

  var DealsView = require('backbone').View.extend({
    el: '#deals',

    initialize: function() {
      this.collection = dealsCollection;
      this.dealViews = {};

      this.collection.on('reset', this.addSeveralDeals, this);
      this.collection.on('add', this.addDeal, this);
      this.collection.on('remove', this.removeDeal, this);

      this.collection.reset(INITIAL_DEALS);
    },

    render: function() {
    },

    addDeal: function(deal) {
      var dealView = new DealView({ model: deal });
      this.$el.append(dealView.render().el);
      this.dealViews[deal.id] = dealView;
    },

    addSeveralDeals: function(deals) {
      var that = this;
      deals.each(function(deal) {
        that.addDeal(deal);
      });
    },

    removeDeal: function(deal) {
      this.dealViews[deal.id].remove();
      delete this.dealViews[deal.id];
    }
  });

  return DealsView;
});

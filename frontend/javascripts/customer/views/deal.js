define(function(require) {
  var _ = require('underscore');
  var template = require('text!../templates/deal.html');
  var openedTemplate = require('text!../templates/deal_opened.html');

  var DealView = require('backbone').View.extend({
    className: 'deal',
    isOpen: false,

    initialize: function() {
      this.listenTo(this.model, 'change', this.refresh);
    },

    events: {
      'click .wrapper': 'open',
      'click .window-wrapper': 'close'
    },

    refresh: function() {
      this.$('.left-data').text(this.model.get('units_left'));
      this.$('.time-data').text(this.model.get('time_left'));
    },

    render: function() {
      this.$el.html(_.template(template, this.model, { variable: 'data' }));
      if (this.isOpen)
        this.open();
      return this;
    },

    initMap: function() {
      var coordinates = this.model.get('coordinates').slice(0);
      coordinates.reverse();
      this.map = L.mapbox.map('opened-map', 'lobodin.map-hslji489').setView(coordinates, 16);
      L.mapbox.markerLayer({
        type: 'Feature',
        geometry: {
            type: 'Point',
            coordinates: this.model.get('coordinates')
        },
        properties: {
            title: this.model.get('business').name,
            description: this.model.get('name'),
            'marker-size': 'medium',
            'marker-color': '#E8330F'
        }
      }).addTo(this.map);
    },

    open: function() {
      this.isOpen = true;
      var tpl = _.template(openedTemplate, this.model, { variable: 'data' });
      this.$('.overlay').html(tpl);
      this.initMap();
    },

    close: function(e) {
      if (e.target != this.$('.window-wrapper')[0] &&
          e.target != this.$('.space')[0])
        return;
      this.isOpen = false;
      this.$('.overlay').empty();
    }
  });

  return DealView;
});

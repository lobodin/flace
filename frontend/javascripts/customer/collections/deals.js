define(function(require) {
  var Deal = require('../models/deal');
  var user = require('../models/user');

  var Deals = require('backbone').Collection.extend({
    model: Deal,
    url: '/api/deals',

    refresh: function() {
      this.fetch({
        data: {
          longitude: user.get('longitude'),
          latitude: user.get('latitude'),
          radius: user.get('radius')
        }
      });
    }
  });

  return new Deals;
});
